import 'package:rune_assist/utils/models/game_type.dart';

class AppIcons {
  GameType game;

  AppIcons(this.game);

  // Menu
  static const farming = "icons/menu/farming.svg";
  static const runecrafting = "icons/menu/runecrafting.svg";
  static const skull = "icons/menu/skull.svg";

  // Rs3
  String get cactus => "icons/${game.pathName}/img_cactus.png";
  String get acornTree => "icons/${game.pathName}/img_acorn_tree.png";
  String get appleTree => "icons/${game.pathName}/img_apple_tree.png";
  String get arbuck => "icons/${game.pathName}/img_arbuck.png";
  String get asgarnian => "icons/${game.pathName}/img_asgarnian.png";
  String get avantoe => "icons/${game.pathName}/img_avantoe.png";
  String get avocado => "icons/${game.pathName}/img_avocado.png";
  String get bananaTree => "icons/${game.pathName}/img_banana_tree.png";
  String get barberry => "icons/${game.pathName}/img_barberry.png";
  String get barley => "icons/${game.pathName}/img_barley.png";
  String get bittercap => "icons/${game.pathName}/img_bittercap.png";
  String get bloodweed => "icons/${game.pathName}/img_bloodweed.png";
  String get butterfly => "icons/${game.pathName}/img_butterfly.png";
  String get cabbage => "icons/${game.pathName}/img_cabbage.png";
  String get cadantine => "icons/${game.pathName}/img_cadantine.png";
  String get cadavaberry => "icons/${game.pathName}/img_cadavaberry.png";
  String get calquatTree => "icons/${game.pathName}/img_calquat_tree.png";
  String get carambolaTree => "icons/${game.pathName}/img_carambola_tree.png";
  String get celastrusTree => "icons/${game.pathName}/img_celastrus_tree.png";
  String get chicken => "icons/${game.pathName}/img_chicken.png";
  String get chinchompa => "icons/${game.pathName}/img_chinchompa.png";
  String get cikuTree => "icons/${game.pathName}/img_ciku_tree.png";
  String get cow => "icons/${game.pathName}/img_cow.png";
  String get crystalTree => "icons/${game.pathName}/img_crystal_tree.png";
  String get curryTree => "icons/${game.pathName}/img_curry_tree.png";
  String get dragon => "icons/${game.pathName}/img_dragon.png";
  String get dragonfruitTree =>
      "icons/${game.pathName}/img_dragonfruit_tree.png";
  String get dragonfruit => "icons/${game.pathName}/img_dragonfruit.png";
  String get dwarfWeed => "icons/${game.pathName}/img_dwarf_weed.png";
  String get dwellberry => "icons/${game.pathName}/img_dwellberry.png";
  String get elderTree => "icons/${game.pathName}/img_elder_tree.png";
  String get fellstalk => "icons/${game.pathName}/img_fellstalk.png";
  String get flyTrap => "icons/${game.pathName}/img_fly_trap.png";
  String get goldenDragonfruit =>
      "icons/${game.pathName}/img_golden_dragonfruit.png";
  String get goutTuber => "icons/${game.pathName}/img_gout_tuber.png";
  String get grapevine => "icons/${game.pathName}/img_grapevine.png";
  String get guam => "icons/${game.pathName}/img_guam.png";
  String get guaranaTree => "icons/${game.pathName}/img_guarana_tree.png";
  String get hammerstone => "icons/${game.pathName}/img_hammerstone.png";
  String get harralander => "icons/${game.pathName}/img_harralander.png";
  String get herb => "icons/${game.pathName}/img_herb.png";
  String get irit => "icons/${game.pathName}/img_irit.png";
  String get jangerberry => "icons/${game.pathName}/img_jangerberry.png";
  String get jute => "icons/${game.pathName}/img_jute.png";
  String get krandorian => "icons/${game.pathName}/img_krandorian.png";
  String get kwuarm => "icons/${game.pathName}/img_kwuarm.png";
  String get lantadyme => "icons/${game.pathName}/img_lantadyme.png";
  String get limpwurt => "icons/${game.pathName}/img_limpwurt.png";
  String get lychee => "icons/${game.pathName}/img_lychee.png";
  String get magicTree => "icons/${game.pathName}/img_magic_tree.png";
  String get mahoganyTree => "icons/${game.pathName}/img_mahogany_tree.png";
  String get mango => "icons/${game.pathName}/img_mango.png";
  String get mapleTree => "icons/${game.pathName}/img_maple_tree.png";
  String get marigold => "icons/${game.pathName}/img_marigold.png";
  String get marrentill => "icons/${game.pathName}/img_marrentill.png";
  String get moneyTree => "icons/${game.pathName}/img_money_tree.png";
  String get morchella => "icons/${game.pathName}/img_morchella.png";
  String get nasturtium => "icons/${game.pathName}/img_nasturtium.png";
  String get onion => "icons/${game.pathName}/img_onion.png";
  String get orangeTree => "icons/${game.pathName}/img_orange_tree.png";
  String get palmTree => "icons/${game.pathName}/img_palm_tree.png";
  String get papayaTree => "icons/${game.pathName}/img_papaya_tree.png";
  String get pineappleTree => "icons/${game.pathName}/img_pineapple_tree.png";
  String get poisonIvy => "icons/${game.pathName}/img_poison_ivy.png";
  String get potatoCactus => "icons/${game.pathName}/img_potato_cactus.png";
  String get potato => "icons/${game.pathName}/img_potato.png";
  String get pricklyPear => "icons/${game.pathName}/img_prickly_pear.png";
  String get rabbit => "icons/${game.pathName}/img_rabbit.png";
  String get ranarr => "icons/${game.pathName}/img_ranarr.png";
  String get redberry => "icons/${game.pathName}/img_redberry.png";
  String get redwoodTree => "icons/${game.pathName}/img_redwood_tree.png";
  String get reed => "icons/${game.pathName}/img_reed.png";
  String get rosemary => "icons/${game.pathName}/img_rosemary.png";
  String get sheep => "icons/${game.pathName}/img_sheep.png";
  String get snapdragon => "icons/${game.pathName}/img_snapdragon.png";
  String get snapegrass => "icons/${game.pathName}/img_snapegrass.png";
  String get spider => "icons/${game.pathName}/img_spider.png";
  String get spiritWeed => "icons/${game.pathName}/img_spirit_weed.png";
  String get stinkfly => "icons/${game.pathName}/img_stinkfly.png";
  String get strawberry => "icons/${game.pathName}/img_strawberry.png";
  String get sunchoke => "icons/${game.pathName}/img_sunchoke.png";
  String get sweetcorn => "icons/${game.pathName}/img_sweetcorn.png";
  String get tarromin => "icons/${game.pathName}/img_tarromin.png";
  String get teakTree => "icons/${game.pathName}/img_teak_tree.png";
  String get toadflax => "icons/${game.pathName}/img_toadflax.png";
  String get tomato => "icons/${game.pathName}/img_tomato.png";
  String get tombshroom => "icons/${game.pathName}/img_tombshroom.png";
  String get torstol => "icons/${game.pathName}/img_torstol.png";
  String get trapper => "icons/${game.pathName}/img_trapper.png";
  String get watermelon => "icons/${game.pathName}/img_watermelon.png";
  String get wergali => "icons/${game.pathName}/img_wergali.png";
  String get whiteLily => "icons/${game.pathName}/img_white_lily.png";
  String get whiteberry => "icons/${game.pathName}/img_whiteberry.png";
  String get wildblood => "icons/${game.pathName}/img_wildblood.png";
  String get willowTree => "icons/${game.pathName}/img_willow_tree.png";
  String get woad => "icons/${game.pathName}/img_woad.png";
  String get yak => "icons/${game.pathName}/img_yak.png";
  String get yanillian => "icons/${game.pathName}/img_yanillian.png";
  String get yewTree => "icons/${game.pathName}/img_yew_tree.png";
  String get zygomite => "icons/${game.pathName}/img_zygomite.png";

  // Other
  static const back = "icons/other/back.svg";
  static const checked = "icons/other/checked.svg";
  static const unchecked = "icons/other/unchecked.svg";
  static const filterActive = "icons/other/filter_active.svg";
  static const filtedInactive = "icons/other/filted_inactive.svg";
  static const grid = "icons/other/grid.svg";
  static const list = "icons/other/list.svg";
  static const play = "icons/other/play.svg";
  static const remove = "icons/other/remove.svg";
  static const stop = "icons/other/stop.svg";
  static const switchGame = "icons/other/switch.svg";
}
