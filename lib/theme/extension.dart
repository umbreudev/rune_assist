import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:rune_assist/theme/custom_colours.dart';
import 'package:rune_assist/theme/custom_text.dart';

extension BuildContextExtension on BuildContext {
  bool get isDarkMode =>
      SchedulerBinding.instance.window.platformBrightness == Brightness.dark;
  MyTextTheme get textTheme => Theme.of(this).extension<MyTextTheme>()!;
  CustomColours get colourTheme => Theme.of(this).extension<CustomColours>()!;
}
