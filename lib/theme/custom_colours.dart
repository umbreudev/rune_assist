import 'package:flutter/material.dart';

@immutable
class CustomColours extends ThemeExtension<CustomColours> {
  const CustomColours({
    required this.primaryBackground,
    required this.secondaryBackground,
    required this.tertiaryBackground,
    required this.quadBackground,
    required this.primaryLabel,
    required this.secondaryLabel,
  });

  final Color? primaryBackground;
  final Color? secondaryBackground;
  final Color? tertiaryBackground;
  final Color? quadBackground;
  final Color? primaryLabel;
  final Color? secondaryLabel;

  @override
  CustomColours copyWith({
    Color? primaryBackground,
    Color? secondaryBackground,
    Color? tertiaryBackground,
    Color? quadBackground,
    Color? primaryLabel,
    Color? secondaryLabel,
  }) {
    return CustomColours(
      primaryBackground: primaryBackground ?? this.primaryBackground,
      secondaryBackground: secondaryBackground ?? this.secondaryBackground,
      tertiaryBackground: tertiaryBackground ?? this.tertiaryBackground,
      quadBackground: quadBackground ?? this.quadBackground,
      primaryLabel: primaryLabel ?? this.primaryLabel,
      secondaryLabel: secondaryLabel ?? this.secondaryLabel,
    );
  }

  @override
  CustomColours lerp(ThemeExtension<CustomColours>? other, double t) {
    if (other is! CustomColours) {
      return this;
    }
    return CustomColours(
      primaryBackground:
          Color.lerp(primaryBackground, other.primaryBackground, t),
      secondaryBackground:
          Color.lerp(secondaryBackground, other.secondaryBackground, t),
      tertiaryBackground:
          Color.lerp(tertiaryBackground, other.tertiaryBackground, t),
      quadBackground: Color.lerp(quadBackground, other.quadBackground, t),
      primaryLabel: Color.lerp(primaryLabel, other.primaryLabel, t),
      secondaryLabel: Color.lerp(secondaryLabel, other.secondaryLabel, t),
    );
  }

  static const Color red = Color(0xFF990505);
  static const Color green = Color(0xFF03B62A);
  static const Color orange = Color(0xFFFFB801);
}
