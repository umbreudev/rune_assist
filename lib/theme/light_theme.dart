import 'package:flutter/material.dart';
import 'package:rune_assist/theme/custom_colours.dart';
import 'package:rune_assist/theme/custom_text.dart';

ThemeData get lightTheme => ThemeData.light().copyWith(
      extensions: <ThemeExtension<dynamic>>[
        lightColours,
        MyTextTheme.light,
      ],
    );

var lightColours = const CustomColours(
  primaryBackground: Color(0xFFC8B996),
  secondaryBackground: Color(0xFFDCCFA9),
  tertiaryBackground: Color(0xFFE2DBC7),
  quadBackground: Color(0xFFEBE8DF),
  primaryLabel: Color(0xFF5D5548),
  secondaryLabel: Color(0xFF92836C),
);
