import 'package:flutter/material.dart';
import 'package:rune_assist/theme/dark_theme.dart';
import 'package:rune_assist/theme/light_theme.dart';

@immutable
class MyTextTheme extends ThemeExtension<MyTextTheme> {
  const MyTextTheme({
    required this.primaryHeading,
    required this.secondaryHeading,
    required this.tertiaryHeading,
    required this.primaryLabel,
    required this.secondaryLabel,
  });

  final TextStyle primaryHeading;
  final TextStyle secondaryHeading;
  final TextStyle tertiaryHeading;
  final TextStyle primaryLabel;
  final TextStyle secondaryLabel;

  @override
  MyTextTheme copyWith({
    TextStyle? primaryHeading,
    TextStyle? secondaryHeading,
    TextStyle? tertiaryHeading,
    TextStyle? primaryLabel,
    TextStyle? secondaryLabel,
  }) {
    return MyTextTheme(
      primaryHeading: primaryHeading ?? this.primaryHeading,
      secondaryHeading: secondaryHeading ?? this.secondaryHeading,
      tertiaryHeading: tertiaryHeading ?? this.tertiaryHeading,
      primaryLabel: primaryLabel ?? this.primaryLabel,
      secondaryLabel: secondaryLabel ?? this.secondaryLabel,
    );
  }

  @override
  MyTextTheme lerp(ThemeExtension<MyTextTheme>? other, double t) {
    if (other is! MyTextTheme) {
      return this;
    }
    return MyTextTheme(
      primaryHeading: TextStyle.lerp(primaryHeading, other.primaryHeading, t)!,
      secondaryHeading:
          TextStyle.lerp(secondaryHeading, other.secondaryHeading, t)!,
      tertiaryHeading:
          TextStyle.lerp(tertiaryHeading, other.tertiaryHeading, t)!,
      primaryLabel: TextStyle.lerp(primaryLabel, other.primaryLabel, t)!,
      secondaryLabel: TextStyle.lerp(secondaryLabel, other.secondaryLabel, t)!,
    );
  }

  static MyTextTheme light = MyTextTheme(
    primaryHeading: TextStyle(
      fontFamily: "RunescapeChat",
      color: lightColours.primaryLabel,
      fontSize: 35,
    ),
    secondaryHeading: TextStyle(
      fontFamily: "RunescapeChat",
      color: lightColours.primaryLabel,
      fontSize: 30,
    ),
    tertiaryHeading: TextStyle(
      fontFamily: "RunescapeChat",
      color: lightColours.primaryLabel,
      fontSize: 20,
    ),
    primaryLabel: TextStyle(
      fontFamily: "RunescapeNpc",
      color: lightColours.primaryLabel,
      fontSize: 25,
    ),
    secondaryLabel: TextStyle(
      fontFamily: "RunescapeNpc",
      color: lightColours.secondaryLabel,
      fontSize: 20,
    ),
  );

  static MyTextTheme dark = MyTextTheme(
    primaryHeading: TextStyle(
      fontFamily: 'RunescapeChat',
      color: darkColours.primaryLabel,
      fontSize: 35,
    ),
    secondaryHeading: TextStyle(
      fontFamily: "RunescapeChat",
      color: darkColours.primaryLabel,
      fontSize: 30,
    ),
    tertiaryHeading: TextStyle(
      fontFamily: "RunescapeChat",
      color: darkColours.primaryLabel,
      fontSize: 20,
    ),
    primaryLabel: TextStyle(
      fontFamily: "RunescapeNpc",
      color: darkColours.primaryLabel,
      fontSize: 25,
    ),
    secondaryLabel: TextStyle(
      fontFamily: "RunescapeNpc",
      color: darkColours.secondaryLabel,
      fontSize: 20,
    ),
  );
}
