import 'package:flutter/material.dart';
import 'package:rune_assist/theme/custom_colours.dart';
import 'package:rune_assist/theme/custom_text.dart';

ThemeData get darkTheme => ThemeData.dark().copyWith(
      textTheme: const TextTheme().apply(fontFamily: "RunescapeNpc"),
      extensions: <ThemeExtension<dynamic>>[
        darkColours,
        MyTextTheme.dark,
      ],
    );

var darkColours = const CustomColours(
  primaryBackground: Color(0xFF0B111A),
  secondaryBackground: Color(0xFF14283C),
  tertiaryBackground: Color(0xFF406488),
  quadBackground: Color(0xFF7199C1),
  primaryLabel: Color(0xFFFFD600),
  secondaryLabel: Color(0xFFFFD600),
);
