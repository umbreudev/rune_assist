import 'package:drift/drift.dart';
import 'package:rune_assist/utils/database/timer/timer_database.dart';
import 'package:rune_assist/utils/database/timer/timer_table.dart';
import 'package:rune_assist/utils/models/game_type.dart';

part 'timer_dao.g.dart';

@DriftAccessor(tables: [RunescapeTimers])
class TodosDao extends DatabaseAccessor<TimerDatabase> with _$TodosDaoMixin {
  TodosDao(TimerDatabase db) : super(db);

  Future<List<RunescapeTimer>> getAllTimers() => select(runescapeTimers).get();

  Future insertTimer(
    String name,
    int duration,
    int level,
    String timeStarted,
    String imgPath,
    GameType gameVersion,
  ) =>
      into(runescapeTimers).insert(
        RunescapeTimersCompanion.insert(
          name: name,
          duration: duration,
          level: level,
          timeStarted: timeStarted,
          imgPath: imgPath,
          gameVersion: gameVersion.index,
        ),
      );

  Future updateTimer(RunescapeTimer timer) =>
      (update(runescapeTimers)..where((t) => t.id.equals(timer.id))).write(
        RunescapeTimersCompanion(
          timeStarted: Value(timer.timeStarted),
        ),
      );

  Future deleteTimer(int timerID) => (delete(runescapeTimers)
        ..where(
          (t) => t.id.equals(timerID),
        ))
      .go();

  Stream<List<RunescapeTimer>> watchAllTimers() =>
      select(runescapeTimers).watch();
}
