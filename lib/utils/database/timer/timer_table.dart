import 'package:drift/drift.dart';

class RunescapeTimers extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  IntColumn get duration => integer()();
  IntColumn get level => integer()();
  TextColumn get timeStarted => text()();
  TextColumn get imgPath => text()();
  IntColumn get gameVersion => integer()();
}
