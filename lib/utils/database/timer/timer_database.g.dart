// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timer_database.dart';

// ignore_for_file: type=lint
class $RunescapeTimersTable extends RunescapeTimers
    with TableInfo<$RunescapeTimersTable, RunescapeTimer> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $RunescapeTimersTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _durationMeta =
      const VerificationMeta('duration');
  @override
  late final GeneratedColumn<int> duration = GeneratedColumn<int>(
      'duration', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _levelMeta = const VerificationMeta('level');
  @override
  late final GeneratedColumn<int> level = GeneratedColumn<int>(
      'level', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _timeStartedMeta =
      const VerificationMeta('timeStarted');
  @override
  late final GeneratedColumn<String> timeStarted = GeneratedColumn<String>(
      'time_started', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _imgPathMeta =
      const VerificationMeta('imgPath');
  @override
  late final GeneratedColumn<String> imgPath = GeneratedColumn<String>(
      'img_path', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _gameVersionMeta =
      const VerificationMeta('gameVersion');
  @override
  late final GeneratedColumn<int> gameVersion = GeneratedColumn<int>(
      'game_version', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, name, duration, level, timeStarted, imgPath, gameVersion];
  @override
  String get aliasedName => _alias ?? 'runescape_timers';
  @override
  String get actualTableName => 'runescape_timers';
  @override
  VerificationContext validateIntegrity(Insertable<RunescapeTimer> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('duration')) {
      context.handle(_durationMeta,
          duration.isAcceptableOrUnknown(data['duration']!, _durationMeta));
    } else if (isInserting) {
      context.missing(_durationMeta);
    }
    if (data.containsKey('level')) {
      context.handle(
          _levelMeta, level.isAcceptableOrUnknown(data['level']!, _levelMeta));
    } else if (isInserting) {
      context.missing(_levelMeta);
    }
    if (data.containsKey('time_started')) {
      context.handle(
          _timeStartedMeta,
          timeStarted.isAcceptableOrUnknown(
              data['time_started']!, _timeStartedMeta));
    } else if (isInserting) {
      context.missing(_timeStartedMeta);
    }
    if (data.containsKey('img_path')) {
      context.handle(_imgPathMeta,
          imgPath.isAcceptableOrUnknown(data['img_path']!, _imgPathMeta));
    } else if (isInserting) {
      context.missing(_imgPathMeta);
    }
    if (data.containsKey('game_version')) {
      context.handle(
          _gameVersionMeta,
          gameVersion.isAcceptableOrUnknown(
              data['game_version']!, _gameVersionMeta));
    } else if (isInserting) {
      context.missing(_gameVersionMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  RunescapeTimer map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return RunescapeTimer(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      duration: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}duration'])!,
      level: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}level'])!,
      timeStarted: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}time_started'])!,
      imgPath: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}img_path'])!,
      gameVersion: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}game_version'])!,
    );
  }

  @override
  $RunescapeTimersTable createAlias(String alias) {
    return $RunescapeTimersTable(attachedDatabase, alias);
  }
}

class RunescapeTimer extends DataClass implements Insertable<RunescapeTimer> {
  final int id;
  final String name;
  final int duration;
  final int level;
  final String timeStarted;
  final String imgPath;
  final int gameVersion;
  const RunescapeTimer(
      {required this.id,
      required this.name,
      required this.duration,
      required this.level,
      required this.timeStarted,
      required this.imgPath,
      required this.gameVersion});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    map['duration'] = Variable<int>(duration);
    map['level'] = Variable<int>(level);
    map['time_started'] = Variable<String>(timeStarted);
    map['img_path'] = Variable<String>(imgPath);
    map['game_version'] = Variable<int>(gameVersion);
    return map;
  }

  RunescapeTimersCompanion toCompanion(bool nullToAbsent) {
    return RunescapeTimersCompanion(
      id: Value(id),
      name: Value(name),
      duration: Value(duration),
      level: Value(level),
      timeStarted: Value(timeStarted),
      imgPath: Value(imgPath),
      gameVersion: Value(gameVersion),
    );
  }

  factory RunescapeTimer.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return RunescapeTimer(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      duration: serializer.fromJson<int>(json['duration']),
      level: serializer.fromJson<int>(json['level']),
      timeStarted: serializer.fromJson<String>(json['timeStarted']),
      imgPath: serializer.fromJson<String>(json['imgPath']),
      gameVersion: serializer.fromJson<int>(json['gameVersion']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'duration': serializer.toJson<int>(duration),
      'level': serializer.toJson<int>(level),
      'timeStarted': serializer.toJson<String>(timeStarted),
      'imgPath': serializer.toJson<String>(imgPath),
      'gameVersion': serializer.toJson<int>(gameVersion),
    };
  }

  RunescapeTimer copyWith(
          {int? id,
          String? name,
          int? duration,
          int? level,
          String? timeStarted,
          String? imgPath,
          int? gameVersion}) =>
      RunescapeTimer(
        id: id ?? this.id,
        name: name ?? this.name,
        duration: duration ?? this.duration,
        level: level ?? this.level,
        timeStarted: timeStarted ?? this.timeStarted,
        imgPath: imgPath ?? this.imgPath,
        gameVersion: gameVersion ?? this.gameVersion,
      );
  @override
  String toString() {
    return (StringBuffer('RunescapeTimer(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('duration: $duration, ')
          ..write('level: $level, ')
          ..write('timeStarted: $timeStarted, ')
          ..write('imgPath: $imgPath, ')
          ..write('gameVersion: $gameVersion')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, name, duration, level, timeStarted, imgPath, gameVersion);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is RunescapeTimer &&
          other.id == this.id &&
          other.name == this.name &&
          other.duration == this.duration &&
          other.level == this.level &&
          other.timeStarted == this.timeStarted &&
          other.imgPath == this.imgPath &&
          other.gameVersion == this.gameVersion);
}

class RunescapeTimersCompanion extends UpdateCompanion<RunescapeTimer> {
  final Value<int> id;
  final Value<String> name;
  final Value<int> duration;
  final Value<int> level;
  final Value<String> timeStarted;
  final Value<String> imgPath;
  final Value<int> gameVersion;
  const RunescapeTimersCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.duration = const Value.absent(),
    this.level = const Value.absent(),
    this.timeStarted = const Value.absent(),
    this.imgPath = const Value.absent(),
    this.gameVersion = const Value.absent(),
  });
  RunescapeTimersCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    required int duration,
    required int level,
    required String timeStarted,
    required String imgPath,
    required int gameVersion,
  })  : name = Value(name),
        duration = Value(duration),
        level = Value(level),
        timeStarted = Value(timeStarted),
        imgPath = Value(imgPath),
        gameVersion = Value(gameVersion);
  static Insertable<RunescapeTimer> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<int>? duration,
    Expression<int>? level,
    Expression<String>? timeStarted,
    Expression<String>? imgPath,
    Expression<int>? gameVersion,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (duration != null) 'duration': duration,
      if (level != null) 'level': level,
      if (timeStarted != null) 'time_started': timeStarted,
      if (imgPath != null) 'img_path': imgPath,
      if (gameVersion != null) 'game_version': gameVersion,
    });
  }

  RunescapeTimersCompanion copyWith(
      {Value<int>? id,
      Value<String>? name,
      Value<int>? duration,
      Value<int>? level,
      Value<String>? timeStarted,
      Value<String>? imgPath,
      Value<int>? gameVersion}) {
    return RunescapeTimersCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      duration: duration ?? this.duration,
      level: level ?? this.level,
      timeStarted: timeStarted ?? this.timeStarted,
      imgPath: imgPath ?? this.imgPath,
      gameVersion: gameVersion ?? this.gameVersion,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (duration.present) {
      map['duration'] = Variable<int>(duration.value);
    }
    if (level.present) {
      map['level'] = Variable<int>(level.value);
    }
    if (timeStarted.present) {
      map['time_started'] = Variable<String>(timeStarted.value);
    }
    if (imgPath.present) {
      map['img_path'] = Variable<String>(imgPath.value);
    }
    if (gameVersion.present) {
      map['game_version'] = Variable<int>(gameVersion.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('RunescapeTimersCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('duration: $duration, ')
          ..write('level: $level, ')
          ..write('timeStarted: $timeStarted, ')
          ..write('imgPath: $imgPath, ')
          ..write('gameVersion: $gameVersion')
          ..write(')'))
        .toString();
  }
}

abstract class _$TimerDatabase extends GeneratedDatabase {
  _$TimerDatabase(QueryExecutor e) : super(e);
  late final $RunescapeTimersTable runescapeTimers =
      $RunescapeTimersTable(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [runescapeTimers];
}
