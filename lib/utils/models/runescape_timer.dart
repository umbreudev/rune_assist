import 'package:rune_assist/utils/models/game_type.dart';
import 'package:rune_assist/utils/models/timer_type.dart';

class RunescapeTimer {
  String name;
  Duration duration;
  int level;
  String iconPath;
  TimerType timerType;
  GameType gameType;

  RunescapeTimer({
    required this.name,
    required this.duration,
    required this.level,
    required this.iconPath,
    required this.timerType,
    required this.gameType,
  });
}
