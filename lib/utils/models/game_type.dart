enum GameType {
  rs3,
  osrs,
  custom;

  String get name {
    switch (this) {
      case GameType.rs3:
        return "Runescape 3";
      case GameType.osrs:
        return "Old School Runescape";
      case GameType.custom:
        return "Custom";
    }
  }

  String get pathName {
    switch (this) {
      case GameType.rs3:
        return "rs3";
      case GameType.osrs:
        return "osrs";
      case GameType.custom:
        return "custom";
    }
  }
}
