import 'package:flutter/material.dart';
import 'package:rune_assist/theme/extension.dart';
import 'package:rune_assist/utils/models/game_type.dart';

class CurrentTimerHeading extends StatelessWidget {
  const CurrentTimerHeading({
    Key? key,
    required this.gameType,
  }) : super(key: key);

  final GameType gameType;

  @override
  Widget build(BuildContext context) => Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(10),
                  topLeft: Radius.circular(10),
                ),
                color: context.colourTheme.secondaryBackground,
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    gameType.name,
                    style: context.textTheme.primaryHeading,
                  ),
                ),
              ),
            ),
          ),
        ],
      );
}
