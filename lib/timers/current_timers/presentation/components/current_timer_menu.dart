import 'package:flutter/material.dart';
import 'package:rune_assist/timers/current_timers/presentation/components/current_timer_cell.dart';
import 'package:rune_assist/timers/current_timers/presentation/components/current_timer_heading.dart';
import 'package:rune_assist/utils/models/game_type.dart';
import 'package:rune_assist/utils/models/runescape_timer.dart';
import 'package:rune_assist/utils/models/timer_type.dart';

class CurrentTimersMenu extends StatelessWidget {
  const CurrentTimersMenu({
    Key? key,
    required this.timerType,
    required this.timers,
  }) : super(key: key);

  final List<RunescapeTimer> timers;
  final TimerType timerType;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        const CurrentTimerHeading(
          gameType: GameType.rs3,
        ),
        Expanded(
          child: MediaQuery.removePadding(
            removeTop: true,
            context: context,
            child: ListView.builder(
              itemCount: timers.length,
              itemBuilder: (context, index) => CurrentTimerCell(
                alternate: index.floor().isEven,
                isLast: index + 1 == timers.length,
                timer: timers[index],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
