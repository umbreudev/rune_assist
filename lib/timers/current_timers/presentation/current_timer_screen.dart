import 'package:flutter/material.dart';
import 'package:rune_assist/routes.dart';
import 'package:rune_assist/theme/extension.dart';
import 'package:rune_assist/theme/icons.dart';
import 'package:rune_assist/timers/current_timers/presentation/components/current_timer_menu.dart';
import 'package:rune_assist/utils/models/game_type.dart';
import 'package:rune_assist/utils/models/runescape_timer.dart';
import 'package:rune_assist/utils/models/timer_type.dart';

class CurrentTimersScreen extends StatelessWidget {
  const CurrentTimersScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: context.colourTheme.primaryLabel,
          ),
          actions: [
            GestureDetector(
              onTap: () => Navigator.pushNamed(context, RouteNames.addTimer),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Icon(
                  Icons.add,
                  color: context.colourTheme.primaryLabel,
                ),
              ),
            )
          ],
        ),
        body: Center(
          child: Container(
            color: context.colourTheme.primaryBackground,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: CurrentTimersMenu(
                timers: [
                  RunescapeTimer(
                    name: "Testing 1",
                    duration: const Duration(minutes: 10),
                    level: 99,
                    iconPath: AppIcons(GameType.rs3).appleTree,
                    timerType: TimerType.allotments,
                    gameType: GameType.rs3,
                  ),
                  RunescapeTimer(
                    name: "Testing 2",
                    duration: const Duration(minutes: 10),
                    level: 99,
                    iconPath: AppIcons(GameType.rs3).appleTree,
                    timerType: TimerType.allotments,
                    gameType: GameType.rs3,
                  ),
                  RunescapeTimer(
                    name: "Testing 3",
                    duration: const Duration(minutes: 10),
                    level: 99,
                    iconPath: AppIcons(GameType.rs3).appleTree,
                    timerType: TimerType.allotments,
                    gameType: GameType.rs3,
                  )
                ],
                timerType: TimerType.allotments,
              ),
            ),
          ),
        ),
      );
}
