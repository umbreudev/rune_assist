import 'package:flutter/material.dart';
import 'package:rune_assist/theme/extension.dart';
import 'package:rune_assist/theme/icons.dart';
import 'package:rune_assist/timers/add_timers/presentation/components/add_timer_menu.dart';
import 'package:rune_assist/utils/models/game_type.dart';
import 'package:rune_assist/utils/models/runescape_timer.dart';
import 'package:rune_assist/utils/models/timer_type.dart';

class AddTimerScreen extends StatelessWidget {
  const AddTimerScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Container(
            color: context.colourTheme.primaryBackground,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: AddTimerMenu(
                timers: [
                  RunescapeTimer(
                    name: "Acorn",
                    duration: const Duration(minutes: 10),
                    level: 99,
                    iconPath: AppIcons(GameType.rs3).acornTree,
                    timerType: TimerType.allotments,
                    gameType: GameType.rs3,
                  ),
                  RunescapeTimer(
                    name: "Testing 2",
                    duration: const Duration(minutes: 10),
                    level: 99,
                    iconPath: AppIcons(GameType.rs3).acornTree,
                    timerType: TimerType.allotments,
                    gameType: GameType.rs3,
                  ),
                  RunescapeTimer(
                    name: "Testing 3",
                    duration: const Duration(minutes: 10),
                    level: 99,
                    iconPath: AppIcons(GameType.rs3).acornTree,
                    timerType: TimerType.allotments,
                    gameType: GameType.rs3,
                  )
                ],
                timerType: TimerType.allotments,
              ),
            ),
          ),
        ),
      );
}
