import 'package:flutter/material.dart';
import 'package:rune_assist/timers/add_timers/presentation/components/add_timer_cell.dart';
import 'package:rune_assist/timers/add_timers/presentation/components/add_timer_heading.dart';
import 'package:rune_assist/utils/models/runescape_timer.dart';
import 'package:rune_assist/utils/models/timer_type.dart';

class AddTimerMenu extends StatelessWidget {
  const AddTimerMenu({
    Key? key,
    required this.timerType,
    required this.timers,
  }) : super(key: key);

  final List<RunescapeTimer> timers;
  final TimerType timerType;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        AddTimerHeading(
          timerType: timerType,
        ),
        Expanded(
          child: MediaQuery.removePadding(
            removeTop: true,
            context: context,
            child: ListView.builder(
              itemCount: timers.length,
              itemBuilder: (context, index) => AddTimerCell(
                alternate: index.floor().isEven,
                isLast: index + 1 == timers.length,
                timer: timers[index],
                onCancelPressed: () {},
                onPlayPressed: () {},
                onStopPressed: () {},
              ),
            ),
          ),
        ),
      ],
    );
  }
}
