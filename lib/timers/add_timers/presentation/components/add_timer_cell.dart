import 'package:flutter/material.dart';
import 'package:rune_assist/theme/extension.dart';
import 'package:rune_assist/utils/models/runescape_timer.dart';

class AddTimerCell extends StatelessWidget {
  const AddTimerCell({
    Key? key,
    required this.alternate,
    required this.timer,
    required this.isLast,
    required this.onPlayPressed,
    required this.onStopPressed,
    required this.onCancelPressed,
  }) : super(key: key);

  final bool alternate;
  final bool isLast;
  final RunescapeTimer timer;
  final Function() onPlayPressed;
  final Function() onStopPressed;
  final Function() onCancelPressed;

  @override
  Widget build(BuildContext context) => Container(
        decoration: BoxDecoration(
          color: alternate
              ? context.colourTheme.tertiaryBackground
              : context.colourTheme.secondaryBackground,
          borderRadius: isLast
              ? const BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                )
              : null,
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    timer.level.toString(),
                    style: context.textTheme.secondaryLabel,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  SizedBox(
                    width: 30,
                    height: 30,
                    child: Image.asset(
                      "assets/${timer.iconPath}",
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    timer.name,
                    style: context.textTheme.primaryLabel,
                  ),
                ],
              ),
              Text(
                "${timer.duration.inMinutes} Minutes",
                style: context.textTheme.secondaryLabel,
              )
            ],
          ),
        ),
      );
}
