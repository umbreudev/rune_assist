import 'package:flutter/material.dart';
import 'package:rune_assist/theme/extension.dart';
import 'package:rune_assist/utils/models/timer_type.dart';

class AddTimerHeading extends StatelessWidget {
  const AddTimerHeading({
    Key? key,
    required this.timerType,
  }) : super(key: key);

  final TimerType timerType;

  @override
  Widget build(BuildContext context) => Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(10),
                  topLeft: Radius.circular(10),
                ),
                color: context.colourTheme.secondaryBackground,
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    timerType.name[0].toUpperCase() +
                        timerType.name.substring(1),
                    style: context.textTheme.primaryHeading,
                  ),
                ),
              ),
            ),
          ),
        ],
      );
}
