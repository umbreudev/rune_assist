import 'package:flutter/material.dart';
import 'package:rune_assist/home_page.dart';
import 'package:rune_assist/routes.dart';
import 'package:rune_assist/setup_firebase.dart';
import 'package:rune_assist/theme/dark_theme.dart';
import 'package:rune_assist/theme/light_theme.dart';
import 'package:rune_assist/timers/add_timers/presentation/add_timer_screen.dart';
import 'package:rune_assist/timers/current_timers/presentation/current_timer_screen.dart';

void main() {
  setupFirebase();
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        RouteNames.addTimer: (_) => const AddTimerScreen(),
        RouteNames.currentTimers: (_) => const CurrentTimersScreen(),
        RouteNames.home: (_) => const HomePage(),
      },
      theme: lightTheme,
      darkTheme: darkTheme,
      initialRoute: RouteNames.home,
    );
  }
}
