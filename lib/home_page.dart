import 'package:flutter/material.dart';
import 'package:rune_assist/routes.dart';
import 'package:rune_assist/theme/extension.dart';

class HomePage extends StatelessWidget {
  const HomePage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(),
        body: Column(
          children: [
            ElevatedButton(
              onPressed: () =>
                  Navigator.pushNamed(context, RouteNames.addTimer),
              child:
                  Text("Add Timers", style: context.textTheme.primaryHeading),
            ),
            ElevatedButton(
              onPressed: () =>
                  Navigator.pushNamed(context, RouteNames.currentTimers),
              child: Text("Current Timers",
                  style: context.textTheme.primaryHeading),
            ),
          ],
        ),
      );
}
