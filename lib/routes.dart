class RouteNames {
  static const addTimer = "/add_timer";
  static const currentTimers = "/current_timers";
  static const home = "/";
}
